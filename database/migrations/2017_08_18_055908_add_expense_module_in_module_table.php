<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpenseModuleInModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $module = new \App\ModuleSetting();
        $module->type = 'employee';
        $module->module_name = 'expenses';
        $module->status = 'active';
        $module->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\ModuleSetting::where('type', 'employee')
                          ->where('module_name', 'expenses')
                          ->delete();
    }
}
