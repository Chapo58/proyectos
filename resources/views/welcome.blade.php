<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Luciano Ciattaglia">
        <link rel="shortcut icon" href="{{ asset('landing/ico/favicon.png') }}">

        <title>Tactical</title>

        <!-- Fonts -->
        {{ Html::style("landing/fonts/open-sans/stylesheet.css") }}
        {{ Html::style("landing/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css") }}
        {{ Html::style("landing/fonts/pe-icon-7-stroke/css/helper.css") }}

        <!-- Styles -->
        {{ Html::style("landing/css/bootstrap.min.css") }}
        {{ Html::style("landing/css/overwrite.css") }}
        {{ Html::style("landing/css/animate.css") }}
        {{ Html::style("landing/css/prettyPhoto.css") }}
        {{ Html::style("landing/css/flexslider.css") }}
        {{ Html::style("landing/css/owl.carousel.css") }}
        {{ Html::style("landing/css/owl.theme.css") }}
        {{ Html::style("landing/css/owl.transitions.css") }}
        {{ Html::style("landing/css/style.css") }}
        {{ Html::style("landing/skins/default.css") }}

    </head>
    <body>
    <!-- Start preloading -->
    <div id="loading" class="loading-invisible">
        <i class="pe-7s-refresh pe-spin pe-3x pe-va"></i><br />
        <p>Por favor aguarde...</p>
    </div>
    <!-- End preloading -->

    <!-- Start header -->
    <header>
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="{{url('landing/img/logo.png')}}" alt="" /></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a id="GoToHome" href="#home" class="selected">Inicio</a></li>
                        <li><a id="GoToFeatures" href="#features">Características</a></li>
                        <li><a id="GoToDesc" href="#description">Descripción</a></li>
                        <li><a id="GoToGallery" href="#screenshot">Capturas</a></li>
                        <li><a id="GoToContact" href="#contact">Contacto</a></li>
                    </ul>
                    <div class="navbar-right">
                        <a href="{{url('/login')}}" class="btn btn-primary">DEMO GRATUITA</a>
                    </div>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </header>
    <!-- End header -->

    <!-- End home -->
    <section id="home" class="home-wrapper parallax image-bg">
        <div class="home-contain">
            <div class="container">
                <div class="row text-center wow fadeInUp" data-wow-delay="0.4s">
                    <div class="col-md-10 col-md-offset-1">
                        <h3>Organiza tu equipo de trabajo y tus proyectos con Tactical</h3>
                        <p class="btn-inline">
                            <a href="{{url('/login')}}" class="btn btn-primary btn-lg">Usa version de prueba ahora</a>
                        </p>
                        <div class="home-slider">
                            <div class="slider-wrapper">
                                <div class="imac-device">
                                    <ul class="slides">
                                        <li>
                                            <a href="#"><img src="{{url('landing/img/screenshot/captura1.png')}}" alt="" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{url('landing/img/screenshot/captura2.png')}}" alt="" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{url('landing/img/screenshot/captura3.png')}}" alt="" /></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <img src="{{url('landing/img/imac.png')}}" class="img-responsive" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End home -->

    <div class="clearfix"></div>

    <!-- Start features -->
    <section id="features" class="contain desc-wrapp gray-bg">
        <div class="container">
            <div class="row text-center wow fadeInUp" data-wow-delay="0.4s">
                <div class="col-md-8 col-md-offset-2">
                    <h3 class="heading"><span>Características</span>Características de Tactical</h3>
                </div>
            </div>
            <div class="row wow fadeInDown" data-wow-delay="0.4s">
                <div class="col-md-4 feature-box">
                    <i class="pe-7s-stopwatch pe-feature"></i>
                    <h5>Control de Tareas y Tiempos</h5>
                    <p>
                        Detalle de tareas pendientes y tiempos de entrega por proyecto de manera gráfica y organizadas en un calendario o arrastrando y soltando en gráficos.
                    </p>
                </div>
                <div class="col-md-4 feature-box">
                    <i class="pe-7s-cash pe-feature"></i>
                    <h5>Control de Ganancias y Gastos</h5>
                    <p>
                        Administración total de las finanzas de cada proyecto y cliente. Emisión de facturas y recibos y cobros online a través del sistema. Impuestos y sueldos.
                    </p>
                </div>
                <div class="col-md-4 feature-box">
                    <i class="pe-7s-tools pe-feature"></i>
                    <h5>Control de Incidensias</h5>
                    <p>
                        Tactical cuenta con un módulo completo de gestión de tickets y reclamos para que usted, su equipo de trabajo, y sus clientes mantengan una comunicación fluida.
                    </p>
                </div>
                <div class="col-md-4 feature-box">
                    <i class="pe-7s-add-user pe-feature"></i>
                    <h5>Control de Asistencias</h5>
                    <p>
                        Gestión total de los horarios de entrada y salida de cada empleado, trabajos desarrollados en lapsos de tiempo específicos y ausencias o demoras diarias.
                    </p>
                </div>
                <div class="col-md-4 feature-box">
                    <i class="pe-7s-note2 pe-feature"></i>
                    <h5>Estimaciones y Presupuestos</h5>
                    <p>
                        Envía estimaciones y presupuestos a tus clientes directamente desde Táctica. Los clientes pueden aceptar o rechazar cada presupuesto o hacer las consultas pertinentes a través del chat interno del sistema.
                    </p>
                </div>
                <div class="col-md-4 feature-box">
                    <i class="pe-7s-graph2 pe-feature"></i>
                    <h5>Roles, permisos, publicaciones, reportes y mucho más!</h5>
                    <p>
                        El sistema más completo de gestión de proyectos del mercado, cada detalle ya ha sido contemplado!
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- End features -->

    <div class="clearfix"></div>

    <!-- Start description -->
    <section id="description" class="contain">
        <div class="container">
            <div class="row">
                <div class="col-md-7 wow fadeInLeft" data-wow-delay="0.4s">
                    <img src="{{url('landing/img/device.png')}}" class="img-responsive" alt="" />
                </div>
                <div class="col-md-5 margintop40 wow fadeInRight" data-wow-delay="0.4s">
                    <div class="accordion clearfix" id="accordion1">
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    <i class="pe-7s-angle-down"></i> Panel de Administración
                                </a>
                            </div>
                            <div id="collapse1" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <p>
                                        Los usuarios administradores tienen acceso a todos los módulos del sistema y todas las configuraciones por defecto. Pueden personalizar la forma gráfica del sistema, definir roles, usuarios y permisos de cada uno. Visualizar todos los informes y editar las asistencias.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    <i class="pe-7s-angle-down"></i> Panel de Empleados
                                </a>
                            </div>
                            <div id="collapse2" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>
                                        Los empleados dependiendo de los permisos que les hayan asignado los administradores también pueden visualizar todos los módulos del sistema (excepto las configuraciones por defecto del mismo).
                                        Los empleados tienen acceso a los proyectos que hayan sido asignados, control de sus asistencias, los tickets de los clientes y las finanzas según su nivel de permisos.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    <i class="pe-7s-angle-down"></i> Panel de Clientes
                                </a>
                            </div>
                            <div id="collapse3" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>
                                        Los clientes pueden solamente visualizar sus proyectos y el estado de los mismos, las tareas pendientes y en las que se están trabajando. Pueden descargar sus facturas y pagarlas a través de Paypal o Stripe en el mismo sistema. Pueden aceptar o rechazar presupuestos y crear tickets con distintas prioridades.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End description -->

    <div class="clearfix"></div>

    <!-- Start download -->
    <section id="download">
        <div class="download-wrapper">
            <div class="container">
                <div class="row wow fadeInUp" data-wow-delay="0.4s">
                    <div class="col-md-8 col-md-offset-2">
                        <h3>Prueba Tactical Gratis!</h3>
                        <p>Ingresa con cualquiera de los 3 usuarios de demostración para probar el sistema</p>
                        <p class="btn-inline">
                            <a href="{{url('/login')}}" class="btn btn-primary btn-lg">DEMO DEL SISTEMA</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End download -->

    <div class="clearfix"></div>

    <!-- Start screenshot -->
    <section id="screenshot" class="contain">
        <div class="container">
            <div class="row text-center wow fadeInUp" data-wow-delay="0.4s">
                <div class="col-md-10 col-md-offset-1 wow fadeInUp" data-wow-delay="0.4s">
                    <h3 class="heading"><span>Screenshots</span> Capturas de pantalla de Tactical</h3>
                </div>
            </div>
        </div>
        <div id="screenshot-contain" class="wow fadeInDown" data-wow-delay="0.4s">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="screenshot-slider">
                            <div class="screenshot-wrapper">
                                <div class="flexslider text-center">
                                    <ul class="slides">
                                        <li>
                                            <a href="#"><img src="{{url('landing/img/screenshot/captura6.png')}}" alt="" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{url('landing/img/screenshot/captura5.png')}}" alt="" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{url('landing/img/screenshot/captura4.png')}}" alt="" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{url('landing/img/screenshot/captura3.png')}}" alt="" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{url('landing/img/screenshot/captura2.png')}}" alt="" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{url('landing/img/screenshot/captura1.png')}}" alt="" /></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <img src="{{url('landing/img/browser.png')}}" class="img-responsive" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End screenshot -->

    <div class="clearfix"></div>

    <!-- Start contact -->
    <section id="contact">
        <div class="contact-contain">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s">
                        <h3 class="heading"><span>Contacto</span>Comuníquese con nosotros</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 wow fadeInDown" data-wow-delay="0.4s">
                        <form id="contactform" action="contact/contact.php" method="post" class="validateform" name="leaveContact">
                            <div class="clearfix"></div>
                            <div id="sendmessage">
                                <div class="alert alert-info marginbot35">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    Su mensaje ha sido enviado. Muchas Gracias!
                                </div>
                            </div>
                            <ul class="listForm">
                                <li>
                                    <i class="pe-7s-users"></i>
                                    <input class="form-control input-name" type="text" name="name" data-rule="required" data-msg="Campo requerido" placeholder="Ingrese su nombre" />
                                    <div class="validation"></div>
                                </li>
                                <li>
                                    <i class="pe-7s-mail"></i>
                                    <input class="form-control input-email" type="text" name="email" data-rule="email" data-msg="Por favor ingrese un email valido" placeholder="Ingrese su email" />
                                    <div class="validation"></div>
                                </li>
                                <li class="push">
                                    <i class="pe-7s-paper-plane"></i>
                                    <textarea class="form-control input-message" rows="6" name="message" data-rule="required" data-msg="Por favor ingrese su mensaje" placeholder="Escriba su mensaje para nosotros"></textarea>
                                    <div class="validation"></div>
                                </li>
                                <li class="push text-center">
                                    <input type="submit" value="Enviar Mensaje" class="btn btn-primary btn-lg" name="Enviar" />
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End contact -->

    <div class="clearfix"></div>

    <!-- Start footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="social-network">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                        <a href="#"><i class="fa fa-skype"></i></a>
                        <a href="#"><i class="fa fa-pinterest"></i></a>
                    </div>
                    <p>2018 &copy; Copyright <a href="https://ciatt.com.ar/" target="_blank">Ciatt Software.</a> Todos los derechos reservados.</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- End footer -->

    {{ Html::script("landing/js/jquery.js") }}
    {{ Html::script("landing/js/bootstrap.min.js") }}
    {{ Html::script("landing/js/jquery-easing-1.3.js") }}

    <script type="text/javascript">
        document.getElementById("loading").className = "loading-visible";
        var hideDiv = function(){document.getElementById("loading").className = "loading-invisible";};
        var oldLoad = window.onload;
        var newLoad = oldLoad ? function(){hideDiv.call(this);oldLoad.call(this);} : hideDiv;
        window.onload = newLoad;
    </script>

    {{ Html::script("landing/js/navigation/waypoints.min.js") }}
    {{ Html::script("landing/js/navigation/jquery.smooth-scroll.js") }}
    {{ Html::script("landing/js/navigation/navbar.js") }}
    {{ Html::script("landing/js/wow/wow.min.js") }}
    {{ Html::script("landing/js/wow/setting.js") }}
    {{ Html::script("landing/js/parallax/jquery.parallax-1.1.3.js") }}
    {{ Html::script("landing/js/parallax/setting.js") }}
    {{ Html::script("landing/js/flexslider/jquery.flexslider.js") }}
    {{ Html::script("landing/js/flexslider/setting.js") }}
    {{ Html::script("landing/js/prettyPhoto/jquery.prettyPhoto.js") }}
    {{ Html::script("landing/js/prettyPhoto/setting.js") }}
    {{ Html::script("landing/js/counters/jquery.appear.js") }}
    {{ Html::script("landing/js/counters/stellar.js") }}
    {{ Html::script("landing/js/counters/setting.js") }}
    {{ Html::script("landing/js/owlcarousel/owl.carousel.js") }}
    {{ Html::script("landing/js/owlcarousel/setting.js") }}
    {{ Html::script("landing/js/totop/jquery.ui.totop.js") }}
    {{ Html::script("landing/js/totop/setting.js") }}
    {{ Html::script("landing/js/validation.js") }}
    {{ Html::script("landing/js/custom.js") }}

    </body>
</html>
